### Aim

In this experiment, the user will learn about Blockchain and its three pillars that are decentralization, transparency & immutability. The simulator will also demonstrate the relation between blocks and chain. Apart from that, he/she will also be able to explain and apply the concepts of blockchain with the help of open and distributed ledger.
### Theory

 <h3>Blockchain Technology:</h3>
                    A blockchain is basically a living list of records, called as "blocks". These blocks are connected to each other by the diverse cryptographic mechanisms. In the category of data structures, this can be related to the concept of a Linked List. In Blockchain, the initial block is known as the "Genesis Block". This naming convention is basically a major commendation to Satoshi Nakamoto. The domain of crypto-currency was pioneered by a bogus naming convention. It can be related to a random scenario of a person or a group of persons, represented by a peculiar name “Satoshi Nakamoto”. In the year 2008, for the purpose of Bitcoin this name was utilized. The technology that was used behind the Bitcoin spectrum was “Block-Chain”. Initially the structure of a block has basically 3 components namely data, hash of current block and hash of previous block. As an illustration in general, the concept of block-chain can be depicted with “m” blocks forming a chain where m can be any random positive integer.
                    <br>
                   <br>
                    <h3>Three pillars of blockchain </h3>
                    <h4>Decenttralization</h4>The true meaning of decentralization is not having a central unit. Now if we take this concept in Blockchain it means that blockchain is autonomous and does not have a central governing unit.
                    <h4>Transparency</h4>Transparency in real life means something with zero opacity. Now if we take this concept in Blockchain, it means that blockchain has zero privacy to be exact when we talk about transactions, all the transactions are public and can be viewed by anyone on the network.
                    <h4>Immutability</h4> Here immutable means exactly what the word means in any real life i.e. something that cannnot be altered. So when we talk about blockchain it means that once a transaction is pushed into blockchain it cannot be altered.
                     <h4>Functioning of Blockchain Technology</h4> ecentralization, Transparency, Immutability are the three pillars of blockchain technology. Efficiency as well as cost can be optimised using this approach. The use as well as request of softwares or applications that are made on blockchain architecture will only advance. A hash can be compared with a fingerprint (that is totally unique). A very popular cryptographic approach that is Secure Hash Algorithm (256) is used to formulate the hash value. Hash Value is basically the amalgamation of the numeric and the alphabetical data. This generation of hash is the primitive approach to understand blockchain. At that instant,when a block is generated, a hash has been produced for the same, and if any change has been done in the block, it will certainly affect the hash value too. With the mechanism of hashing, the changes are easily identified. The ultimate verdict within the block is the hash value from a predecessor. Fortunately, by the means of this a chain of blocks is created that is the strategy behind blockchain's architecture.
                    
                    


<h3>Procedure</h3>
                        <h4>Steps of simulator :</h4><br>
                        1.&nbsp;First complete the recall task as per the instructions given on the page.Then click on next button on the top of the page.. <br>
                        2.&nbsp;To Understand the concept of open ledger, Enter the Name and Amount of the sender as well as the recipient in the placeholder. <br>
                        3.&nbsp; Click on the Submit button to complete the details of a particular user. Complete the same process for the next user. <br>
                        4.&nbsp;In the canvas section, the illustration will take place according to the inputs given by the user. <br>
                        5.&nbsp;Click on the Next Experiment button to proceed further. <br>
                        6.&nbsp; The next section is of Distributed ledger where the concept of decentralization is implemented. <br>
                        7.&nbsp; Click on the desired block in the ledger, then click on the desired user where you need to place that block. <br>
                        8.&nbsp;The same process is done for the next two users.<br>
                        9.&nbsp;Click on the validate button to validate your transaction.
                        10.&nbsp; The next concept is of immutability, where the user wil click on the toggle button, to display or delete a block.
                        11. &nbsp;The next concept is of immutability, where the user wil click on the toggle button, to display or delete a block.
