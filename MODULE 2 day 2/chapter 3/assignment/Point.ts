function trianglefun() {
    let a1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let a2: HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    let b1: HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
    let b2: HTMLInputElement = <HTMLInputElement>document.getElementById("t4");
    let c1: HTMLInputElement = <HTMLInputElement>document.getElementById("t5");
    let c2: HTMLInputElement = <HTMLInputElement>document.getElementById("t6");
    let p1: HTMLInputElement = <HTMLInputElement>document.getElementById("t7");
    let p2: HTMLInputElement = <HTMLInputElement>document.getElementById("t8");

    var x1: number = +a1.value;
    var y1: number = +a2.value;
    var x2: number = +b1.value;
    var y2: number = +b2.value;
    var x3: number = +c1.value;
    var y3: number = +c2.value;
    var x: number = +p1.value;
    var y: number = +p2.value;

    if (isNaN(x1)) {
        alert("Please enter a number");
    }

    else if (isNaN(y1)) {
        alert("Please enter a number");
    }

    else if (isNaN(x2)) {
        alert("Please enter a number");
    }

    else if (isNaN(y2)) {
        alert("Please enter a number");
    }

    else if (isNaN(x3)) {
        alert("Please enter a number");
    }

    else if (isNaN(y3)) {
        alert("Please enter a number");
    }
    else if (isNaN(x)) {
        alert("Please enter a number");
    }

    else if (isNaN(y)) {
        alert("Please enter a number");
    }

    else {
        var AREA: number = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);

        var area1: number = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
        var area2: number = Math.abs((x * (y1 - y3) + x1 * (y3 - y) + x3 * (y1 - y)) / 2);
        var area3: number = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2);

        var sum: number = area1 + area2 + area3;

        if (Math.abs(AREA - sum) < 0.000001) {
            document.getElementById("ans").innerHTML = ("Point lies INSIDE the TRIANGLE");
        }
        else {
            document.getElementById("ans").innerHTML = ("Point lies OUTSIDE the TRIANGLE");
        }
    }
}